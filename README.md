<div align="center">
    <img src="https://raw.githubusercontent.com/Jobsity/ReactChallenge/main/src/assets/jobsity_logo_small.png"/>
</div>

# Flask Challenge

## Description

This project retrieves the most recent Stock's close-value given their stock code, as well, exposes the user's query history and shows the top 5 most-requested Stocks (this is only available for admin users).  



## Architecture



![Architecture Diagram](diagram.svg)



This web application is composed by two Flask microservices. The first one is a *Stock Service* which its single responsability is to given a stock code (`stock_symbol`) retrieve from a third party service (**Stooq**) the current Stock information. Such information is consumed, processed and exposed to an user by the second service. This second service name is  *Api Service*, is the is the user-facing service and it handles the authentication, the stocks value retrieval and their storage; also uses a Redis in-memory database for caching emphimerally the not found stocks. The intention of this volatile list is to avoid hitting the database and the *Stock Service* more than once within a short period if a given stock code was not found.



### API Service

As descripted, this is a Flask microservice responsible of the user authentication, data retrieval and its persistence. All this is possible thanks to Flask, PyJWT, SQLAlchemy, Redis, Marshmallow, Requests and last but not least, ApiSpec, an OpenAPI (Swagger v3) documentation generator.



#### Endpoints 

The operations mentioned are exposed via RESTful APIs, you might explore their documentation via `http://{api-service-host:port}/api/docs`. If you run it locally it'd be `http://localhost:5008/api/docs` . 



Briefly these are:



- `POST /api/v1/user/authenticate`: This is the door of the application. You should consume this by `Basic Auth` using the credentials given in next sections and putting the response token value among the HTTP Headers at the headder `x-access-tokens` . **IMPORTANT:** the next endpoints demand the presence of this token, otherwise you get a **Forbiden (403)** status code. Example response:

  ```json
  {
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfaWQiOjEsImV4cCI6MTY0MTg1MDAyN30.SblHK4z3Kggijz6exMuE6ushXqozT04AWAf-lreh8JA"
  }
  ```

  

- `GET /api/v1/stock`: The heart of the application. This endpoint given and `stock_symbol` ([in the world of stocks, a stock abbreviation is known as symbol](https://en.wikipedia.org/wiki/Ticker_symbol)) query parameter retrieves the most recent stock closing value. Example response:

  ```json
  {
    "symbol": "WIG20",  
    "company_name": "WIG20",
    "quote": 2291.23  
  }
  ```

- `GET /api/v1/stock/history`: Returns all the queries done by the current user. Example response:

  ```json
  [
    {
      "stock": {
        "name": "WIG20",
        "high": 2328.41,
        "open": 2311.34,
        "close": 2291.23,
        "low": 2289.59,
        "date": "2022-01-10",
        "symbol": "WIG20",
        "time": "17:09:45"
      },
      "query_date": "2022-01-10T20:57:25.108671"
    },
    {
      "stock": {
        "name": "WIG20",
        "high": 2314.16,
        "open": 2309.61,
        "close": 2311.93,
        "low": 2278.63,
        "date": "2022-01-07",
        "symbol": "WIG20",
        "time": "17:09:45"
      },
      "query_date": "2022-01-09T04:24:58.492628"
    }
  ]
  ```

  

- `GET /api/v1/stats`: Returns the top 5 most-queried stocks. **IMPORTANT:** This is only available for users with **ADMIN** role. Example response:

  ```json
  [
    {
      "stock": "AAPL.US",
      "times_requested": 6
    },
    {
      "stock": "NQ.F",
      "times_requested": 4
    },
    {
      "stock": "WIG20",
      "times_requested": 3
    },
    {
      "stock": "TSLA.US",
      "times_requested": 1
    },
    {
      "stock": "WIG",
      "times_requested": 1
    }
  ]
  ```

  

- `POST /api/v1/user`: Adds an user given a JSON Request Body as follow:

  ```json
  {
      "username": "demouser",
      "email": "test@test",
      "password": "demopassword",
      "active": 1,
      "role": "USER"
  }
  ```

   And answers back this confirmation message:

  ```json
  {
    "status": "success",
    "message": "Registered successfully"
  }
  ```



That's briefly it, further details and different scenarios are present at the Swagger UI at `/api/docs`.

### Stock Service

This service harvest Stooq.com. It's responsible of given a `stock_symbol`  retrieve a CSV containing all the stock details. This is an internal service and asumes that it's being consumed by an application that already did the authentication part, therefore this endpoint doesn't requires a JWT token at the HTTP Header.



#### Endpoints

As said, this service only returns the current stock information and this is exposed via a RESTful API, you might explore the documentation via `http://{stock-service-host:port}/api/docs`. If you run it locally it'd be `http://localhost:5001/api/docs` . As a resume it goes this way:

- `GET /api/v1/stock`: Given a `stock_symbol` query parameter the response is the following:

  ```json 
  {
    "status": "success",
    "message": "",
    "data": {
      "low": 39716.04,
      "high": 42280.0,
      "date": "2022-01-10",
      "open": 42205.36,
      "symbol": "BTC.V",
      "name": "BITCOIN",
      "time": "17:11:52",
      "close": 41042.82
    }
  }
  ```


## Running the Project

This project can be run initially in two ways: via **docker-compose**  (heavily suggested) and via **console**. Also this was designed to work at different environments with the support of the different configuration files explained at the next section.



### Configuration

Each microservice has its own configuration files which are:

- `.flaskenv`: Common running configuration. Is the default configuration file. It's consumed internally during testing.
- `.testenv`: Testing related configuration. Keys present here overlaps `.flaskenv` configuration. 
- `.env`: Production related configuration. Keys present here overlaps `.flaskenv` configuration. To use this configuration the environment variable `ENV` must be `Production`.



Important Keys:

```ini
DEBUG=false
# To let Flask know that it's running in production mode.
FLASK_ENV=production
# The port the application is running on when running via console. 
PORT=5008

# The endpoint for the stock service. Present only
# at the api service configuration.
STOCK_SERVICE_ENDPOINT="http://stock-service:5001/api/v1"
# The database URI. Present only at the api service
# configuration.
SQLALCHEMY_DATABASE_URI=sqlite:///db.sqlite3
# The redis URI. Present only at the api service 
# configuration.
REDIS_URL="redis://redis:6379/0"
```



To expose in Docker the HTTP interface of each service, Gunicorn is used. Thus, the configuration of this at each service is placed at `{api,stock}_service/gunicorn-cfg.py`  and its entries are:



```py
bind = '0.0.0.0:5008'
workers = 2
accesslog = '-'
loglevel = 'info'
capture_output = True
enable_stdio_inheritance = True
```



The most relevant configuration here is the `bind` parameter which is the one that defines the port that gunicorn uses to expose the service.



### Credentials

This project initializes with two users: `admin` and `johndoe` with `ADMIN` and `USER` role respectively.  Their credentials are:

-  user: `admin`, password: `admin`
- user: `johndoe`, password: `john`

You can add as many users as you want by using the `POST /api/v1/users` endpoint. 



**IMPORTANT:** These users have entries already at the `db.sqlite3` database, so you can right away start observing the queries that they've done and the stats as well. 

### Docker-Compose

All this is glued by Docker with **Docker-Compose** for a *one-click-deployment*. You can observe that at each service there's a `Dockerimage` file that packs each service into a container which later is used by the `docker-compose.yml` (present at the top of the repo) to load each service and the Redis instance.



To run this a pre-requisite is to have docker and docker-compose installed on your system. After this, if you're running on a Unix environment, execution permission must be granted to the script `run_docker.sh` which is present at the root of the repo and run it. It goes this way:



```bash
# grant execution permission, this is required only one time
$ chmod +x run_docker.sh
# execute it
$ ./run_docker.sh
```



If you're on windows you just do:

```bash
$ ./run_docker.cmd
```



### Console

If you do not have docker-compose or just don't want to use it, you can do the following steps:

1. Start a Redis instance, either locally, a self hosted, you choose it.
1. Identify the URI you'd use, i.e.: `redis://my-running-instance:6379/2`
1. Update the **REDIS_URL** entry at `api_service/.env`. Currently it's `REDIS_URL="redis://redis:6379/0"`. Given the step 2, that must be now:  `REDIS_URL="redis://my-running-instance:6379/2"`
1. Run the next commands:

```bash
# Create a virtualenv and activate it if you haven't
$ python -m venv virtualenv
$ . venv/bin/activate

# Install dependencies
$ pip install -r requirements.txt

$ cd api_service
# Init the database
$ flask db migrate
$ flask db upgrade

# Set the environment variables
export FLASK_APP=wsgi.py
export FLASK_ENV=Production
export ENV=Production

# Start the api service
$ flask run

# At another console, Start the stock service
$ cd stock_service

# As this is another session, environment
# variables must be set again
export FLASK_APP=wsgi.py
export FLASK_ENV=Production
export ENV=Production

$ flask run
```



**Important:** These commands assumes that you're at an Unix environment, if it's at windows, most commands remains the same, but the environment variables. The approach would be:

1. Setting each of the environment variables.

   ```bash
   # Setting a variable in CMD is as follow:
   $ setx VARIABLE "VALUE"
   
   # For instance:
   $ setx FLASK_APP "wsgi.py"
   ```

2. Exit the console.

3. Open a new terminal and run all of the above commands but the environment variables.



## Testing the code

This project was heavily tested using `PyTest` , `Mock` and `MockRedisPy`. To run the test and observe that everything is workgin you should: Install the dependencies and testing dependencies (a pattern to avoid bloating the docker images with non-production libraries), exporting the Flask environment variables and declaring the environment as testing environment as follow:

```bash
# Setting up the environment

$ python -m venv virtualenv
$ . venv/bin/activate
$ pip install -r requirements.txt
$ pip install -r requirements.dev.txt

# Exporting the Flask variables
$ export FLASK_APP=wsgi.py
$ export ENV=Testing

# Running the test at each project

# API Service
$ cd api_service
$ pytest ./test/ --cov --cov-report term --cov-report html

# Stock Service
$ cd ..

$ cd stock_service
$ pytest ./test/ --cov --cov-report term --cov-report html

# Going back to the root of the project
cd ..
```



Here's an ouput of how it'd look like.

**API Service:**

![API Service tests](img/test_api_service.png)

**Stock Service:**![Stock Service tests](img/test_stock_service.png)

That's pretty much it. Track and enjoy the stocks.


***

**The original documentation start at this point**



# Flask Challenge

## Description
This project is designed to test your knowledge of back-end web technologies, specifically in the Flask framework, Rest APIs, and decoupled services (microservices).

## Assignment
The goal of this exercise is to create a simple API using Flask to allow users to query [stock quotes](https://www.investopedia.com/terms/s/stockquote.asp).

The project consists of two separate services:
* A user-facing API that will receive requests from registered users asking for quote information.
* An internal stock aggregator service that queries external APIs to retrieve the requested quote information.

For simplicity, both services will share the same dependencies (requirements.txt) and can be run from the same virtualenv, but remember that they are still separate processes.

## Minimum requirements
### API service
* Endpoints in the API service should require authentication (no anonymous requests should be allowed). Each request should be authenticated via Basic Authentication.
You have to implement the code to check the user credentials are correct and put the right decorators around resource methods (check the auth.helpers module).
* When a user makes a request to get a stock quote (calls the stock endpoint in the api service), if a stock is found, it should be saved in the database associated to the user making the request.
* The response returned by the API service should be like this:

  `GET /stock?q=aapl.us`
  ```json
    {
    "symbol": "AAPL.US",
    "company_name": "APPLE",
    "quote": 123
    }
  ```
  The quote value should be taken from the `close` field returned by the stock service.
* A user can get his history of queries made to the api service by hitting the history endpoint. The endpoint should return the list of entries saved in the database, showing the latest entries first:
  
  `GET /history`
  
  ```json
  [
      {"date": "2021-04-01T19:20:30Z", "name": "APPLE", "symbol": "AAPL.US", "open": "123.66", "high": 123.66, "low": 122.49, "close": "123"},
      {"date": "2021-03-25T11:10:55Z", "name": "APPLE", "symbol": "AAPL.US", "open": "121.10", "high": 123.66, "low": 122, "close": "122"},
      ...
  ]
  ```
* A super user (and only super users) can hit the stats endpoint, which will return the top 5 most requested stocks:

  `GET /stats`
  
  ```
  [
      {"stock": "aapl.us", "times_requested": 5},
      {"stock": "msft.us", "times_requested": 2},
      ...
  ]
  ```
* All endpoint responses should be in JSON format.

### Stock service
* Assume this is an internal service, so requests to endpoints in this service don't need to be authenticated.
* When a stock request is received, this service should query an external API to get the stock information. For this challege, use this API: `https://stooq.com/q/l/?s={stock_code}&f=sd2t2ohlcvn&h&e=csv`.
* Note that `{stock_code}` above is a parameter that should be replaced with the requested stock code.
* here: https://stooq.com/t/?i=518

## Architecture
![Architecture Diagram](diagram.svg)
1. A user makes a request asking for Apple's current Stock quote: `GET /stock?q=aapl.us`
2. The API service calls the stock service to retrieve the requested stock information
3. The stock service delegates the call to the external API, parses the response, and returns the information back to the API service.
4. The API service saves the response from the stock service in the database.
5. The data is formatted and returned to the user.

## Bonuses
The following features are optional to implement, but if you do, you'll be ranked higher in our evaluation process.
* Add unit tests for the bot and the main app.
* Connect the two services via RabbitMQ instead of doing http calls.
* Use JWT instead of basic authentication for endpoints.

## How to run the project
* Create a virtualenv: `python -m venv virtualenv` and activate it `. virtualenv/bin/activate`.
* Install dependencies: `pip install -r requirements.txt`
* Start the api service: `cd api_service ; flask db migrate; flask db upgrade ; flask run`
* Start the stock service: `cd stock_service ; flask run`

__Important:__ If your implementation requires different steps to start the services
(like starting a rabbitMQ consumer), document them here!