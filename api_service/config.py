"""
Custom settings for the application
---
It's expected that the environment was loaded previously
"""

from os import environ

SECRET_KEY = environ.get('SECRET_KEY')
# The database variable might not be loaded during migrations
SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI')
SQLALCHEMY_TRACK_MODIFICATIONS = environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')
STOCK_SERVICE_ENDPOINT = environ.get('STOCK_SERVICE_ENDPOINT')
REDIS_URL = environ.get('REDIS_URL')
REDIS_STOCK_TTL = environ.get('REDIS_STOCK_TTL')
