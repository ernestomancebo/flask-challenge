"""JWT generator"""

from functools import wraps

import jwt
from flask import request, current_app
from api_service.models import User


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        TOKEN_HEADER = 'x-access-tokens'

        if TOKEN_HEADER not in request.headers:
            return {"status": "error", "message": "Authentication Token is missing"}, 403

        token = request.headers[TOKEN_HEADER]

        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'], "HS256")
            current_user = User.query.filter_by(id=data['public_id']).first()
        except:
            return {"status": "error", "message": "A valid Token is missing"}, 403

        return f(*args, current_user, **kwargs)

    return decorator
