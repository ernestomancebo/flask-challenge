"""OpenAPI v3 Specification"""

from api_service.api.schemas import (StatsSchema, StockHistorySchema,
                                     StockInfoSchema, StockQuerySchema,
                                     UserSchema)
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin

spec = APISpec(title="Stock API", version="1.0.0",
               openapi_version="3.0.2", plugins=[FlaskPlugin(), MarshmallowPlugin()])

# Security Scheme
jwt_scheme = {"type": "http", "in": "header",
              "name": "x-access-tokens", "bearerFormat": "JWT"}

spec.components.security_scheme("jwt", jwt_scheme)

spec.components.schema("StockInfo", schema=StockInfoSchema)
spec.components.schema("User", schema=UserSchema)
spec.components.schema("QueryHistory", schema=StockHistorySchema)
spec.components.schema("StockQuery", schema=StockQuerySchema)
spec.components.schema("Stats", schema=StatsSchema)

# Tags for Enpoint annotations
tags = [
    {"name": "User Functions", "description": "Operations related to the Users"},
    {"name": "Stock Functions", "description": "Operations related to the Stocks"},
    {"name": "Stats Functions", "description": "Operations related to the Statss"},
]

for t in tags:
    spec.tag(t)
