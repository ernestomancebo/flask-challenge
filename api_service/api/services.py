"""Services for resources/repositories"""
import json
from urllib.parse import urlencode

import requests
from datetime import datetime
from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from redis import Redis
from sqlalchemy import desc, func
from api_service.api.schemas import StockSchema
from api_service.models import Stock, StockQuery, User


class StockService():

    def __init__(self, db: SQLAlchemy):
        self.db = db

    def get_stock(self, stock_symbol: str):
        """Return the stock queried today"""
        query = Stock.query.filter_by(
            symbol=stock_symbol, entry_date=datetime.today().date())
        stock = query.first()

        return stock

    def add_stock(self, stock: Stock):
        self.db.session.add(stock)
        self.db.session.commit()


class StockHistoryService():

    def __init__(self, db: SQLAlchemy):
        self.db = db

    def add_stock_query(self, stock: Stock, user: User):
        stock_query = StockQuery(None, stock.id, user.id, None)
        self.db.session.add(stock_query)
        self.db.session.commit()

    def get_stock_query_history(self, user: User):
        user_queries = self.db.session.query(StockQuery).join(
            Stock).filter(StockQuery.user_id == user.id).order_by(
                desc(StockQuery.date)).all()

        return user_queries

    def get_stock_query_stats(self):
        count = self.db.session.query(Stock.symbol, func.count(StockQuery.user_id).label('times_requested')).join(
            Stock).group_by(Stock.symbol).order_by(desc('times_requested')).limit(5).all()

        return count


class RedisService():

    def __init__(self, redis: Redis):
        self.redis = redis
        self.not_found_stocks = 'NOT_FOUND_STOCKS'

    def is_not_found_stock(self, stock_symbol: str):
        return self.redis.sismember(self.not_found_stocks, stock_symbol)

    def add_not_found_stock(self, stock_symbol: str):
        self.redis.sadd(self.not_found_stocks,  stock_symbol)

        # If hasn't been set, put an expiration to the not found key.
        # Maybe the service couldn't retrieve it or it was listed later.
        ttl = self.redis.ttl(self.not_found_stocks)
        if not ttl or ttl == -1:
            self.redis.expire(self.not_found_stocks,
                              int(current_app.config.get('REDIS_STOCK_TTL')))


class StockServiceClient():

    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.stock_schema = StockSchema()

    def get_stock(self, stock_symbol):
        res = requests.get(self.__get_url(stock_symbol))
        stock = None

        if res.status_code == 200:
            data = json.loads(res.content.decode('utf-8'))['data']
            stock = self.stock_schema.load(data)

        return res.status_code, stock

    def __get_url(self, stock_symbol):
        params = {'stock_symbol': stock_symbol}
        url = f'{self.endpoint}/stock?{urlencode(params)}'

        return url


class UserService():
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def add_user(self, user: User):
        self.db.session.add(user)
        self.db.session.commit()

    def get_by_username(self, username: str):
        user = User.query.filter_by(username=username).first()
        return user
