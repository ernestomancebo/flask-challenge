"""Marshmallow Schemas"""

# encoding: utf-8

from marshmallow import fields
from marshmallow.decorators import post_load
from marshmallow.validate import Length
from api_service.extensions import ma
from api_service.models import Stock, StockQuery, User


class StockInfoSchema(ma.Schema):
    symbol = fields.String(dump_only=True)
    company_name = fields.String(dump_only=True, attribute="name")
    quote = fields.Float(dump_only=True, attribute="close")


class StatsSchema(ma.Schema):
    stock = fields.String(dump_only=True)
    times_requested = fields.Integer(dump_only=True)


class UserSchema(ma.Schema):
    id = fields.Integer(required=False)
    username = fields.String(validate=Length(max=80), required=True)
    email = fields.String(validate=Length(max=80),  required=True)
    password = fields. String(validate=Length(max=255), required=True)
    active = fields.Boolean(required=False, dump_default=True)
    role = fields.String(validate=Length(max=20), required=True)

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)


class StockSchema(ma.Schema):
    """Stock schema response got when a request is successful"""
    symbol = fields.String()
    name = fields.String()
    date = fields.Date()
    time = fields.Time()
    open = fields.Float()
    high = fields.Float()
    low = fields.Float()
    close = fields.Float()

    @post_load
    def make_stock(self, data, **kwargs):
        return Stock(**data)


class StockHistorySchema(ma.Schema):
    query_date = fields.DateTime(dump_only=True, attribute='date')
    stock = fields.Nested(StockSchema, dump_only=True, attribute='stock')


class StockQuerySchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    stock_id = fields.String(dump_only=True)
    user_id = fields.String(dump_only=True)
    date = fields.DateTime(dump_only=True)

    @post_load
    def make_StockQuery(self, data, **kwargs):
        return StockQuery(**data)
