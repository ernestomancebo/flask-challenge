"""Contains the resources used by Flask-Restful"""

import json
from datetime import datetime, timedelta

import jwt
from flask import abort, current_app, request
from flask.helpers import make_response
from flask_restful import Resource

from api_service.api.schemas import (StatsSchema, StockHistorySchema,
                                     StockInfoSchema, UserSchema)
from api_service.api.services import (RedisService, StockHistoryService,
                                      StockService, StockServiceClient,
                                      UserService)
from api_service.auth.token_required import token_required
from api_service.extensions import db, pwd_context, redis
from api_service.models import User


class StockQuery(Resource):
    """
    Endpoint to allow users to query stocks
    """

    def __init__(self):
        # Services
        self.redis = RedisService(redis)
        self.stock_service = StockService(db)
        self.stock_history_service = StockHistoryService(db)
        self.stock_service_client = StockServiceClient(
            current_app.config['STOCK_SERVICE_ENDPOINT'])

        # Schemas
        self.stock_schema = StockInfoSchema()

    @token_required
    def get(self, current_user):
        """
        ---
        description: Retrieves current stock
        parameters:
            - in: query
              name: stock_symbol
              schema:
                type: string
              description: Stock Symbol to look up
        security:
            - jwt: []
        responses:
            "200":
                description: Call Successful
                content:
                    application/json:
                        schema: Stock
            "400":
                description: Bad Request. Invalid parameters.
            "404":
                description: Stock not found. Given stock_symbol was not found.
        tags:
            - Stock Functions
        """
        stock_symbol = request.args.get('stock_symbol', None, str)

        if not stock_symbol or len(stock_symbol.strip()) == 0:
            return "Invalid query", 400

        stock_symbol = stock_symbol.upper()
        if self.redis.is_not_found_stock(stock_symbol):
            return {"status": "error", "message": f"Stock '{stock_symbol}' was not found"}, 404

        # Look up for any entry done today
        stock = self.stock_service.get_stock(stock_symbol)

        if not stock:
            status, stock = self.stock_service_client.get_stock(stock_symbol)

            if status == 200:
                # Persist the Stock
                stock.id = None
                self.stock_service.add_stock(stock)
            else:
                if status == 404:
                    # Avoids hitting the service several times
                    self.redis.add_not_found_stock(stock_symbol)
                    message = f"Stock '{stock_symbol}' was not found"
                else:
                    # Something went wrong
                    message = "Something went wrong, try again later"

                return {"status": "error", "message": message}, status

        # Persist the query for further statistic
        self.stock_history_service.add_stock_query(stock, current_user)

        data = self.stock_schema.dump(stock)
        return data, 200


class History(Resource):

    def __init__(self):
        self.stock_history_service = StockHistoryService(db)
        self.stock_history_schema = StockHistorySchema()

    @token_required
    def get(self, current_user):
        """
        ---
        description: Returns queries made by current user.
        security:
            - jwt: []
        responses:
            "200":
                description: Call Successful
                content:
                    application/json:
                        schema: QueryHistory
        tags:
            - Stats Functions
        """
        query_history = self.stock_history_service.get_stock_query_history(
            current_user)
        query_history_response = self.stock_history_schema.dump(
            query_history, many=True)

        return query_history_response, 200


class Stats(Resource):

    def __init__(self):
        self.stock_history_service = StockHistoryService(db)
        self.stats_schema = StatsSchema()

    @token_required
    def get(self, current_user: User):
        """
        ---
        description: Allows admin users to see which are the most queried stocks.
        security:
            - jwt: []
        responses:
            "200":
                description: Call Successful
                content:
                    application/json:
                        schema: Stats
            "403":
                description: Bad Request. Unauthorized
        tags:
            - Stats Functions
        """
        if current_user.role != 'ADMIN':
            return {"status": "error", "message": "Unauthorized"}, 403

        stats = self.stock_history_service.get_stock_query_stats()
        stats = [{'stock': x[0], 'times_requested': x[1]} for x in stats]
        stats = self.stats_schema.dump(stats, many=True)

        return stats, 200


class AuthenticateResource(Resource):
    """Authentication related Operation"""

    def __init__(self):
        self.user_schema = UserSchema()
        self.user_service = UserService(db)

    def post(self):
        """
        ---
        description: Authenticates an user
        responses:
            "200":
                description: Login Successful
            "401":
                description: Bad Request. Invalid parameters.
        tags:
            - User Functions
        """
        auth = request.authorization

        if not auth or not auth.username or not auth.password:
            return make_response('could not verify', 401, {'WWW.Authentication': 'Basic realm: "login required"'})

        user: User = self.user_service.get_by_username(auth.username)
        if pwd_context.verify(auth.password, user.password):
            token = jwt.encode({'public_id': user.id,
                                'exp': datetime.utcnow() + timedelta(minutes=30)}, current_app.config['SECRET_KEY'])

            return {'token': token}, 200

        return make_response('could not verify', 401, {'WWW.Authentication': 'Basic realm: "login required"'})


class UserResource(Resource):
    """User related operations"""

    def __init__(self):
        self.user_schema = UserSchema()
        self.user_service = UserService(db)

    @token_required
    def post(self, current_user):
        """
        ---
        description: Allow admin user to create other users
        security:
            - jwt: []
        parameters:
            - in: body
              name: data
              required: true
              schema:
                type: User
              description: User object to be created. The field id is not required.
        responses:
            "201":
                description: User Created
            "401":
                description: Bad Request. Invalid parameters.
            "403":
                description: Bad Request. Unauthorized.
        tags:
            - User Functions
        """

        if current_user.role != 'ADMIN':
            return {"status": "error", "message": "Unauthorized"}, 403

        j = json.loads(bytes.decode(request.data))
        # This field is expected by the schema.
        # As we're creating, default this to zero
        j['id'] = 0
        errors = self.user_schema.validate(j)

        if errors:
            abort(401, str(errors))

        # Setting this back to None, thus
        # the db engine generates the id
        j['id'] = None
        user = User(**j)
        self.user_service.add_user(user)

        return {'status': 'success', 'message': 'Registered successfully'}, 201
