"""empty message

Revision ID: 8af1f56db626
Revises: fd2569ce7fc3
Create Date: 2022-01-08 23:03:03.213907

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8af1f56db626'
down_revision = 'fd2569ce7fc3'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
