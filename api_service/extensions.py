"""Extensions initialization"""
# encoding: utf-8

from os import environ

from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_redis import FlaskRedis
from flask_sqlalchemy import SQLAlchemy
from mockredis import MockRedis
from passlib.context import CryptContext

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
pwd_context = CryptContext(schemes=["pbkdf2_sha256"], deprecated="auto")
# This way we can mock the Redis server
redis = FlaskRedis() if not environ.get('ENV', 'Development') else FlaskRedis.from_custom_provider(MockRedis)
