"""Application factory"""
# encoding: utf-8

from os import environ, path

from dotenv import load_dotenv
from flask import Flask

from api_service import api
from api_service.api.swagger import SWAGGER_URL, swagger_ui_blueprint
from api_service.extensions import db, migrate, redis


def create_app(testing=False):

    config_mode = environ.get('ENV', 'Development').capitalize()
    basedir = path.abspath(path.dirname(__file__))

    if testing:
        # Some stuffs are required for testing
        load_dotenv(path.join(basedir, '.testenv'))
    elif config_mode == 'Production':
        # Replace environment variables with
        # production variables
        load_dotenv(path.join(basedir, '.env'))
    load_dotenv(path.join(basedir, '.flaskenv'))

    app = Flask("api_service")
    app.config.from_pyfile('config.py')

    app.logger.info(f'Configuration    = {app.config}')

    configure_extensions(app)
    register_blueprints(app)

    return app


def configure_extensions(app: Flask):
    db.init_app(app)
    migrate.init_app(app, db)
    redis.init_app(app)


def register_blueprints(app: Flask):
    app.register_blueprint(api.views.blueprint)
    app.register_blueprint(swagger_ui_blueprint, url_prefix=SWAGGER_URL)
