# Flask related configuration
DEBUG=true
FLASK_ENV=development
FLASK_APP=app:create_app
PORT=5008
FLASK_RUN_PORT=5008
SQLALCHEMY_TRACK_MODIFICATIONS=false
REDIS_STOCK_TTL=300

# Development configuration
SECRET_KEY=S3cret
STOCK_SERVICE_ENDPOINT=http://localhost:5001/api/v1
SQLALCHEMY_DATABASE_URI=sqlite:///db.sqlite3
REDIS_URL=redis://localhost:6379/0