#!/usr/bin/env bash

cd api_service/

# Apply Migrations
flask db upgrade

# Init users
python manage.py init

cd ..

gunicorn --config api_service/gunicorn-cfg.py api_service.wsgi:flask_app