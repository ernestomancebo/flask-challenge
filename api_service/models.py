"""Database models"""

# encoding: utf-8

import datetime

from sqlalchemy import (Boolean, Column, Date, DateTime, Float, Integer,
                        String, Time)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey

from api_service.extensions import db, pwd_context


class User(db.Model):
    """Basic user model"""

    def __init__(self, id, username, email, password, active, role):
        self.id = id
        self.username = username
        self.email = email
        self.password = password
        self.active = active
        self.role = role

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String(80), unique=True, nullable=False)
    email = Column(String(80), unique=True, nullable=False)
    _password = Column("password", String(255), nullable=False)
    active = Column(Boolean, default=True)
    role = Column(String(20), nullable=False)

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, value):
        self._password = pwd_context.hash(value)

    def __repr__(self):
        return "<User %s>" % self.username


class Stock(db.Model):
    """Stock Model"""

    __tablename__ = 'stock'

    id = Column(Integer, primary_key=True)
    symbol = Column(String(), nullable=False)
    name = Column(String(), nullable=False)
    date = Column(Date())
    time = Column(Time())
    open = Column(Float())
    high = Column(Float())
    low = Column(Float())
    close = Column(Float())
    entry_date = Column(Date(), default=datetime.datetime.today)

    def __repr__(self) -> str:
        return f'<Stock {self.name}>'


class StockQuery(db .Model):
    """Holds the API hits thus we can extract the statistics"""

    __tablename__ = 'stock_query'

    def __init__(self, id, stock_id, user_id, date):
        self.id = id
        self.stock_id = stock_id
        self.user_id = user_id
        self.date = date

    id = Column(Integer, primary_key=True)
    stock_id = Column(String(), ForeignKey('stock.id'))
    user_id = Column(String(), ForeignKey('user.id'))
    date = Column(DateTime, default=datetime.datetime.utcnow)

    # Relationship
    stock = relationship('Stock')
    user = relationship('User')
