# encoding: utf-8

import click
from flask.cli import with_appcontext


@click.group()
def cli():
    """Main entry point"""
    pass


@cli.command("create-db")
@with_appcontext
def init_db():
    """Create tables, if don't exist"""
    from api_service.extensions import db

    click.echo("creating tables if don't exist")
    db.create_all()


@cli.command("init")
@with_appcontext
def init():
    """Create a new admin user"""
    from api_service.extensions import db
    from api_service.models import User

    users = [
        User(id=None, username="admin", email="admin@mail.com",
             password="admin", active=True, role='ADMIN'),
        User(id=None, username="johndoe", email="johndoe@mail.com",
             password="john", active=True, role='USER')
    ]

    for u in users:
        existing_user = User.query.filter(User.username == u.username).first()
        if not existing_user:
            click.echo(f"create {u.username} user")
            db.session.add(u)
            db.session.commit()

    click.echo("created users.")


if __name__ == "__main__":
    cli()
