# encoding: utf-8

import csv
import os
from datetime import datetime

import pytest
from flask_sqlalchemy import SQLAlchemy
from api_service.app import create_app
from api_service.extensions import db as _db
from api_service.models import User


@pytest.fixture()
def client():
    """Creates a client for Flask"""
    test_app = create_app(True)
    test_app.config['TESTING'] = True
    test_app.testing = True

    test_client = test_app.test_client()

    with test_app.app_context():
        # Something to load
        _db.create_all()
        add_users(_db)
        load_mock_data(_db)

    yield test_client


def add_users(db: SQLAlchemy):
    users = [
        User(id=None, username="admin", email="admin@mail.com",
             password="admin", active=True, role='ADMIN'),
        User(id=None, username="johndoe", email="johndoe@mail.com",
             password="john", active=True, role='USER')
    ]

    for u in users:
        existing_user = User.query.filter(User.username == u.username).first()
        if not existing_user:
            db.session.add(u)
            db.session.commit()


def load_mock_data(db: SQLAlchemy):
    from api_service.models import Stock, StockQuery
    schema_dict = {
        'stock.csv': Stock,
        'stock_query.csv': StockQuery
    }

    for x in schema_dict.keys():
        src_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "mock_data", x
        )
        rows = __load_csv(src_path, schema_dict[x])
        db.session.bulk_save_objects(rows)

    db.session.commit()


def __load_csv(src_path, model):
    with open(src_path) as file:
        dict_reader = csv.DictReader(file, delimiter=',')
        list_result = [model(**__parse_date_fields(x))
                       for x in list(dict_reader)]

    return list_result


def __parse_date_fields(dict_result: dict):
    has_time = dict_result.get('time', None)
    if dict_result.get('date', None):
        # If doesn't has a time field, then asume is within the date
        format = '%Y-%m-%d' if has_time else '%Y-%m-%d %H:%M:%S.%f'

        dict_result['date'] = datetime.strptime(
            dict_result['date'], format)

    # If doesn't has time field
    if has_time:
        dict_result['time'] = datetime.strptime(
            dict_result['time'], '%H:%M:%S.%f').time()

    return dict_result
