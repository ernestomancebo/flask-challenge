"""Contains all the tests related to the resources at api_service"""

import json
from urllib.parse import urlencode

from flask.testing import FlaskClient
from mock import patch

BASE_PATH = '/api/v1'
BAD_STOCK_SYMBOL_RESPONSE = b'{\n    "status": "error",\n    "message": "stock_symbol \'LULU\' not found",\n    "data": null\n}\n'
NEW_ENTRY_RESPONSE = b'{\n    "status": "success",\n    "message": "",\n    "data": {\n        "time": "17:13:52",\n        "symbol": "BTC.V",\n        "name": "BITCOIN",\n        "date": "2022-01-10",\n        "close": 41120.0,\n        "high": 42280.0,\n        "low": 39716.04,\n        "open": 42205.36\n    }\n}\n'


def get(client: FlaskClient, path, params='', headers={}):
    url = f'{path}?{urlencode(params)}'
    response = client.get(url, headers=headers)

    return response


def post(client: FlaskClient, path, data=None, auth=(), params='', headers={}):
    url = f'{path}?{urlencode(params)}'
    response = client.post(url, data=data, auth=auth, headers=headers)

    return response


def test_load_token(client, user='admin', password='admin'):
    authenticate_path = f'{BASE_PATH}/user/authenticate'
    # JWT token is composed by three segments
    token_length = 3

    response = post(client, authenticate_path, auth=(user, password))
    # Perssist session token
    session_token = response.json['token']
    assert len(session_token.split('.')) == token_length

    return session_token


def test_empty_login(client):
    authenticate_path = f'{BASE_PATH}/user/authenticate'
    response = post(client, authenticate_path, auth=('', ''))

    assert response.status_code == 401


def test_get_stock_no_token(client):
    stock_path = f'{BASE_PATH}/stock'
    params = {'stock_symbol': 'wig20'}
    response = get(client, path=stock_path, params=params)

    # Token is required
    assert response.status_code == 403


def test_get_evil_token(client):
    stock_path = f'{BASE_PATH}/stock'
    params = {'stock_symbol': ''}
    token = test_load_token(client)
    headers = {
        'x-access-tokens': 'something.evil'
    }
    response = get(client, path=stock_path,
                   params=params, headers=headers)

    # Evil request
    assert response.status_code == 403


def test_get_stock_blakn(client):
    stock_path = f'{BASE_PATH}/stock'
    params = {'stock_symbol': ''}
    token = test_load_token(client)
    headers = {
        'x-access-tokens': token
    }

    # with patch('requests.get') as mock_request:
    #     # Stooq always returns 200
    #     mock_request.return_value.status_code = 404
    #     mock_request.return_value.content = BAD_STOCK_SYMBOL_RESPONSE

    response = get(client, path=stock_path,
                   params=params, headers=headers)

    # Evil request
    assert response.status_code == 400


def test_get_stock_unexistent(client):
    stock_path = f'{BASE_PATH}/stock'
    params = {'stock_symbol': 'Lulu'}
    token = test_load_token(client)
    headers = {
        'x-access-tokens': token
    }

    with patch('requests.get') as mock_request:
        # Stooq always returns 200
        mock_request.return_value.status_code = 404
        mock_request.return_value.content = BAD_STOCK_SYMBOL_RESPONSE

        response = get(client, path=stock_path,
                       params=params, headers=headers)

        # Such stock doesn't exists
        assert response.status_code == 404


def test_get_stock_unexistent_twice(client):
    stock_path = f'{BASE_PATH}/stock'
    params = {'stock_symbol': 'Lulu'}
    token = test_load_token(client)
    headers = {
        'x-access-tokens': token
    }

    with patch('requests.get') as mock_request:
        # Stooq always returns 200
        mock_request.return_value.status_code = 404
        mock_request.return_value.content = BAD_STOCK_SYMBOL_RESPONSE

        response = get(client, path=stock_path,
                       params=params, headers=headers)

        # Such stock doesn't exists
        assert response.status_code == 404
    # Call it again, so we test the redis volaitile store
    response = get(client, path=stock_path,
                   params=params, headers=headers)
    assert response.status_code == 404


def test_get_stock_valid(client):
    stock_path = f'{BASE_PATH}/stock'
    params = {'stock_symbol': 'aapl.us'}
    token = test_load_token(client)
    headers = {
        'x-access-tokens': token
    }

    response = get(client, path=stock_path, params=params, headers=headers)
    assert response.status_code == 200


def test_get_stock_valid_new_entry(client):
    stock_path = f'{BASE_PATH}/stock'
    params = {'stock_symbol': 'btc.v'}
    token = test_load_token(client)
    headers = {
        'x-access-tokens': token
    }

    with patch('requests.get') as mock_request:
        # Stooq always returns 200
        mock_request.return_value.status_code = 200
        mock_request.return_value.content = NEW_ENTRY_RESPONSE

        response = get(client, path=stock_path,
                       params=params, headers=headers)

        assert response.status_code == 200


def test_get_stock_history(client):
    stock_path = f'{BASE_PATH}/user/history'
    token = test_load_token(client)
    headers = {
        'x-access-tokens': token
    }

    response = get(client, path=stock_path, headers=headers)
    assert response.status_code == 200

    response = response.json
    # There're 13 entries at the mock CSV file
    assert len(response) == 13


def test_get_stock_stats_not_admin(client):
    stock_path = f'{BASE_PATH}/stats'
    token = test_load_token(client, user='johndoe', password='john')
    headers = {
        'x-access-tokens': token
    }

    response = get(client, path=stock_path, headers=headers)
    # Forbidden request
    assert response.status_code == 403


def test_get_stock_stats_admin(client):
    stock_path = f'{BASE_PATH}/stats'
    token = test_load_token(client)
    headers = {
        'x-access-tokens': token
    }

    response = get(client, path=stock_path, headers=headers)
    assert response.status_code == 200

    response = response.json
    # We only expect the top 5 queried stocks
    assert len(response) == 5


def test_add_user(client):
    from api_service.models import User
    from api_service.api.schemas import UserSchema

    schema = UserSchema()
    pwd = 'S3cretPass'
    u = User(
        id=None, username='testuser', email='test@test', password=pwd, active=1, role='USER'
    )

    path = f'{BASE_PATH}/user'
    token = test_load_token(client)
    headers = {
        'x-access-tokens': token
    }

    data = schema.dump(u)
    data['password'] = pwd
    response = post(client, path=path, data=json.dumps(data),
                    auth=None, headers=headers)
    assert response.status_code == 201
