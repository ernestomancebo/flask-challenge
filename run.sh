#!/bin/bash

source env/bin/activate

export ENV=Production
export FLASK_APP=wsgi.py
export FLASK_ENV=development

cd stock_service && flask run && cd -