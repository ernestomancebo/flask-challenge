"""Default configuration

Use env var to override
"""
import os

from decouple import config


class Config(object):
    basedir = os.path.abspath(os.path.dirname(__file__))

    SECRET_KEY = config("SECRET_KEY", default='S3cret')
    ENV = config("FLASK_ENV", "development")
    PORT = config("PORT", 5001, int)

    DEBUG = ENV == 'development'


class ProductionConfig(Config):
    DEBUG = False


class DebugConfig(Config):
    DEBUG = True


config_dict = {
    'Production': ProductionConfig,
    'Debug': DebugConfig}
