"""Bootstraps the Stock Service Flask APP"""

from decouple import config
from flask import Flask, jsonify

from stock_service.api.spec import spec
from stock_service.api.swagger import API_URL
from stock_service.app import create_app
from stock_service.config import config_dict


def init_documentation_spec(app: Flask):
    """Adds OpenAPI v3 documentation"""

    # inits documentations/specs
    with app.test_request_context():
        for fn_name in app.view_functions:
            if fn_name == 'static':
                continue

            view_fn = app.view_functions[fn_name]
            spec.path(view=view_fn)

        # Includes Swagger endpoint
        @app.route(API_URL)
        def create_swagger_spec():
            return jsonify(spec.to_dict())


DEBUG = config('DEBUG', default=True, cast=bool)
config_mode = 'Debug' if DEBUG else 'Production'

try:
    app_config = config_dict[config_mode.capitalize()]

except KeyError:
    exit('Error: invalid <config_mode>. Expected values: [Debug, Production]')

flask_app = create_app(app_config)

# Put in here due to circular dependency if done at factory
init_documentation_spec(flask_app)

if DEBUG:
    flask_app.logger.info(f'DEBUG          = {DEBUG}')
    flask_app.logger.info(f'Environment    = {config_mode}')

if __name__ == '__main__':
    flask_app.run(port=app_config['PORT'])
