from flask import Flask

from stock_service import api
from stock_service.api.swagger import SWAGGER_URL, swagger_ui_blueprint


def create_app(config):
    app = Flask("stock_service")
    app.config.from_object(config)

    register_blueprints(app)

    return app


def register_blueprints(app: Flask):
    app.register_blueprint(api.views.blueprint)
    app.register_blueprint(swagger_ui_blueprint, url_prefix=SWAGGER_URL)
