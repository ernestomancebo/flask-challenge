"""Contains all the tests related to the resources"""

import json
from urllib.parse import urlencode

from mock import patch

# Stock URL
STOCK_PATH_URL = '/api/v1/stock'

# HTTP Status Codes
OK_STATUS_CODE = 200
BAD_REQUEST_STATUS_CODE = 400
NOT_FOUND_STATUS_CODE = 404
STOOQ_ERROR_STATUS_CODE = 500

# Stooq Responses
# pylint: disable=line-too-long
INVALID_SYMBOL_RESPONSE = b'Symbol,Date,Time,Open,High,Low,Close,Volume,Name\r\nXYX,N/D,N/D,N/D,N/D,N/D,N/D,N/D,XYX\r\n'
# pylint: disable=line-too-long
NICE_SYMBOL_RESPONSE = b'Symbol,Date,Time,Open,High,Low,Close,Volume,Name\r\nAAPL.US,2022-01-06,16:46:48,172.6335,175.3,172.11,173.38,23152285,APPLE\r\n'


def get(client, path, params=''):
    """Wraps an HTTP GET operation"""
    url = f'{path}?{urlencode(params)}'
    response = client.get(url)

    return response


def test_get_none_stock(client):
    """Sends a GET request with no parameters. Must fail"""

    res = get(client, STOCK_PATH_URL)
    assert res.status_code == BAD_REQUEST_STATUS_CODE


def test_get_empty_stock(client):
    """Sends a GET request with no parameter. Turns into an invalid request"""

    res = get(client, STOCK_PATH_URL)
    assert res.status_code == BAD_REQUEST_STATUS_CODE


def test_get_bad_stock(client):
    """Sends an invalid stock symbol. Such request must fail"""

    with patch('requests.get') as mock_request:
        # Stooq always returns 200
        mock_request.return_value.status_code = OK_STATUS_CODE
        mock_request.return_value.content = INVALID_SYMBOL_RESPONSE

        params = {'stock_symbol': 'xyz'}
        res = get(client, STOCK_PATH_URL, params)

    # Validates the resource status code
    assert res.status_code == NOT_FOUND_STATUS_CODE


def test_get_service_down(client):
    """Sends a valid symbol but the service is down."""

    with patch('requests.get') as mock_request:
        # Stooq always returns 200
        mock_request.return_value.status_code = STOOQ_ERROR_STATUS_CODE
        mock_request.return_value.content = INVALID_SYMBOL_RESPONSE

        params = {'stock_symbol': 'AAPL.US'}
        res = get(client, STOCK_PATH_URL, params)

    # Validates the resource status code
    assert res.status_code == STOOQ_ERROR_STATUS_CODE


def test_get_nice_stock(client):
    """Sends a nice request. Must return a 200 status code and a valid JSON object"""
    # /api/v1
    with patch('requests.get') as mock_request:
        mock_request.return_value.status_code = OK_STATUS_CODE
        mock_request.return_value.content = NICE_SYMBOL_RESPONSE

        params = {'stock_symbol': 'AAPL.US'}
        res = get(client, STOCK_PATH_URL, params)

    assert res.status_code == OK_STATUS_CODE

    content = json.loads(res.data.decode('utf-8'))
    assert 'APPLE' == content['data']['name']
    assert 173.38 == content['data']['close']
