"""Configuration/Boostrap for PyTest"""

import pytest
from stock_service.config import DebugConfig


@pytest.fixture
def client():
    """Creates a client for Flask"""
    from stock_service.app import create_app
    test_config = DebugConfig()

    # Init app in test mode
    test_app = create_app(test_config)
    test_app.config['TESTING'] = True
    test_app.testing = True

    test_client = test_app.test_client()

    with test_app.app_context():
        # Something to load
        pass

    yield test_client
