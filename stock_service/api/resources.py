# encoding: utf-8
"""Contains the resources used by Flask-Restful"""

import csv
from datetime import datetime

import requests
from flask import request
from flask_restful import Resource
from stock_service.api.schemas import StockSchema


class StockResource(Resource):
    """
    Endpoint that is in charge of aggregating the stock
    information from external sources and returning them to
    our main API service. Currently we only get the data from a
    single external source: the stooq API.
    """

    def get(self):
        """
        ---
        description: Returns the current stock value given its stock_symbol
        parameters:
            - in: query
              name: stock_symbol
              schema:
                type: string
              description: Stock Symbol to look up
        responses:
            "200":
                description: Call Successful
                content:
                    application/json:
                        schema: Stock
            "400":
                description: Bad Request. Invalid parameters.
            "404":
                description: Stock not found. Given stock_symbol was not found.
        tags:
            - Stooqs Functions
        """
        stock_symbol = request.args.get('stock_symbol', None, str)

        if not stock_symbol or stock_symbol.strip() == '':
            return self.__error_result("Missing parameter 'stock_symbol'"), 400

        # with requests.Session() as reqest_session:
        request_url = f"https://stooq.com/q/l/?s={stock_symbol}&f=sd2t2ohlcvn&h&e=csv"
        response = requests.get(request_url)

        if response.status_code != 200:
            return self.__error_result(
                f"Server returned status code: '{response.status_code}'"), 500

        content = response.content.decode('utf-8')
        stock_data_obj = self.__extract_content_from_csv(content)

        # This happens when the given key is not valid
        if stock_data_obj['Open'] == 'N/D':
            return self.__error_result(f"stock_symbol '{stock_symbol}' not found"), 404

        stock_data_obj = self.__parse_dict_result(stock_data_obj)
        stock_data_obj = self.__parse_date_fields(stock_data_obj)

        schema = StockSchema()
        print(stock_data_obj)

        return self.__success_result("", schema.dump(stock_data_obj)), 200

    def __extract_content_from_csv(self, csv_content):
        dict_reader = csv.DictReader(csv_content.splitlines(), delimiter=',')
        list_result = list(dict_reader)

        return list_result[0]

    def __parse_dict_result(self, dict_result: dict):
        dict_result = {key.lower(): value for key,
                       value in dict_result.items()}

        return dict_result

    def __parse_date_fields(self, dict_result: dict):
        dict_result['date'] = datetime.strptime(
            dict_result['date'], '%Y-%m-%d')
        dict_result['time'] = datetime.strptime(
            dict_result['time'], '%H:%M:%S').time()

        return dict_result

    def __status_response(self, status, message, data):
        return {
            "status": status,
            "message": message,
            "data": data
        }

    def __error_result(self, message, data=None):
        return self.__status_response("error", message, data)

    def __success_result(self, message, data=None):
        return self.__status_response("success", message, data)
