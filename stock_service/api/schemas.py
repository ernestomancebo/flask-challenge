# encoding: utf-8
"""Contains all the schemas"""

from marshmallow import fields
from stock_service.extensions import marsh


class StockSchema(marsh.Schema):
    """Stock schema response sent when a request is successful"""
    symbol = fields.String(dump_only=True)
    name = fields.String(dump_only=True)
    date = fields.Date(dump_only=True)
    time = fields.Time(dump_only=True)
    open = fields.Float(dump_only=True)
    high = fields.Float(dump_only=True)
    low = fields.Float(dump_only=True)
    close = fields.Float(dump_only=True)
