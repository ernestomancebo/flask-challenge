"""OpenAPI v3 Specification"""

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from stock_service.api.schemas import StockSchema

spec = APISpec(title="Stooq App", version="1.0.0",
               openapi_version="3.0.2", plugins=[FlaskPlugin(), MarshmallowPlugin()])
spec.components.schema("Stock", schema=StockSchema)

# Tags for Enpoint annotations
tags = [
    {"name": "Stooqs Functions", "description": "Stooq Service consumer"},
]

for t in tags:
    spec.tag(t)
